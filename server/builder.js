'use strict';

/**
 * The 2 HTTP methods allowed for the Twilio webhook.
 */
const HTTP_METHODS = ['GET','POST'];

/** The Core of the Twilio-Response-Builder module */
class TwilioBuilder {
    /**
     * @constructor
     * @param {String} route The custom route for the webhook URL 
     * @param {String} method HTTP Method, either GET or POST
     * @param {*} smsResponse The response to be sent to the Twilio server. Will be casted to a string 
     */
    constructor(route, method, smsResponse){
        if (!route.startsWith('/')){
            throw new Error('Route must be started with a slash(/)');
        }

        /**
         * @property Custom route for the webhook URL
         */
        this.route = route;
        
        if (HTTP_METHODS.indexOf(method.toUpperCase())>=0)
            this.method = method;
        else {
            throw new Error('Method has to be GET or POST');
        }

        /**
         * @prop  Get the SMS response in plain text format
         */
        this.smsText = smsResponse.toString();

        if (this.smsText==null || this.smsText.length===0)
            throw new Error('Warning: SMS Response is empty');

        var smsXMLResponse = `
            <Response>
                <Message>
                    ${this.smsText}
                </Message>
            </Response>
        `;

        /** 
         * @prop Get the SMS response in the XML format
        */
        this.smsXMLResponse = smsXMLResponse;

        /**
         * @prop a callback function to be mounted on Express/Loopback routers
         * @param {*} req - request
         * @param {*} res - response to be sent to the Twilio server 
         */
        this.requestCallback = (req,res)=>{
            res.setHeader('content-type','application/xml');
            res.send(this.smsXMLResponse);
            res.end();
        };

        
    }
}

module.exports = TwilioBuilder;