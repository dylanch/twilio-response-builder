Twilio-Response-Builder is a package for building SMS response before connecting with the Twilio webhook. It provides the SMS response in XML format for sending to the Twilio server, and a callback for mounting on Express/Loopback custom routers.

For Loopback Router:
```javascript
var builder = require('twilio-response-builder');
var client = new builder(
    '/test',
    'GET',
    'hello world'
);

module.exports = function(app){
    if (client.method==='GET'){
        app.get(client.route, client.requestCallback);
    }
    else if (client.method==='POST'){
        app.post(client.route,client.requestCallback);
    }
}
```

For Express.js Router:

```javascript
var twilio = require('twilio-response-builder');
var client = new twilio(
    '/test',
    'GET',
    'hello world'
);
app.route(client.route)
.get(client.requestCallback);
```
If you only need to extract the XML response 

(Notice: since we only need the client.smsXMLResponse property, the first 2 parameters of the constructor don't really matter. However, the first parameter (route : String) must be started with a '/' and the second parameter (method : String) must be either 'GET' or 'POST', as of version 1.1.2.):

```javascript
app.get('/test', function(res,req){

    var smsResponse = 'hello world';

    var twilio = require('twilio-response-builder');

    var client = new twilio('///', 'GET',smsResponse); 
    // send message HERE
    res.setHeader('content-type','application/xml');
    res.send(client.smsXMLResponse);
});
```

Connect the webhook with your application like this in Messaging -> A Message Comes In:
![](https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAA08AAAAJDZmYzk5ZjVlLWI1MGMtNDc5ZC1iYjczLTE0ZjJlZDhkYWFjMg.png)

Change /[your custom route] to client.route and the HTTP request drop down to client.method. In the Express example above, we should change the method to HTTP GET:
![](https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAxDAAAAJDM3M2UwOGViLTI3MjAtNDhiNS05YTcwLWE1ZjAxZmNmYzQ0OA.png)

Click Save. And now you can test the server:
![](https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAuCAAAAJDU5ZDVjZDk3LWVhODctNDdjOC05Mzk5LTcxMzkwZDBhM2MwZg.jpg)